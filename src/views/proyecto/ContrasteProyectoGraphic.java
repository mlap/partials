package views.proyecto;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DefaultKeyedValues2DDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/**
 * A population pyramid demo.
 */
public class ContrasteProyectoGraphic extends ApplicationFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 199603558100999567L;
	/**
     * Creates a new demo.
     *
     * @param title  the frame title.
     */
    public ContrasteProyectoGraphic(String title) {
        super(title);
        JPanel chartPanel = createDemoPanel();
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
    }

    public static JFreeChart createChart(CategoryDataset dataset) {
        JFreeChart chart = ChartFactory.createStackedBarChart(
            "Proyectos Comparados",
            "Materiales",     // domain axis label
            "Parametros Porcentuales", // range axis label
            dataset,         // data
            PlotOrientation.HORIZONTAL,
            true,            // include legend
            true,            // tooltips
            false            // urls
        );
        return chart;
    }

    /**
     * Creates a dataset.
     *
     * @return A dataset.
     */
    public static CategoryDataset createDataset() {
        DefaultKeyedValues2DDataset data = new DefaultKeyedValues2DDataset();
        data.addValue(-6.0, "Comparado", "Costo de Materiales");
        data.addValue(-8.0, "Comparado", "Ganancia Estimada");
        data.addValue(-11.0, "Comparado", "Duraci�n del Proyecto en Meses");
        data.addValue(-13.0, "Comparado", "Margen de Ganancia");
        
        data.addValue(10.0, "Seleccionado", "Costo de Materiales");
        data.addValue(12.0, "Seleccionado", "Ganancia Estimada");
        data.addValue(13.0, "Seleccionado", "Duraci�n del Proyecto en Meses");
        data.addValue(14.0, "Seleccionado", "Margen de Ganancia");
        
        return data;
    }

    public static JPanel createDemoPanel() {
        JFreeChart chart = createChart(createDataset());
        ChartPanel panel = new ChartPanel(chart);
        return panel;
    }
    /**
     * Starting point for the demonstration application.
     *
     * @param args  ignored.
     */
    public static void main(String[] args) {
    	ContrasteProyectoGraphic demo = new ContrasteProyectoGraphic(
                "Contraste de Proyectos");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

}